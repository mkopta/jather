module InputParser where

import Data
import Text.ParserCombinators.Parsec
--import qualified Text.ParserCombinators.Parsec.Token as P
import Text.ParserCombinators.Parsec.Language(haskellStyle)

parseInput :: String -> String -> Either ParseError [Class]
parseInput = parse inputParser

inputParser :: GenParser Char a [Class]
inputParser = many classDef

classDef :: GenParser Char a Class
classDef =
  do
    reserved "Class"
    n <- identifier
    reserved "extends"
    e <- identifier
    reserved "{"
    fs <- many field
    c <- contructor
    ms <- many method
    reserved "}"
    return (Class n e fs c ms)

field :: GenParser Char a Field
field =
  do
    n <- identifier
    f <- identifier
    return (Field n f)

constructor :: GenParser Char a Constructor
constructor =
  do
    n <- identifier
    reserved "("
    ps <- many param
    reserved ")"
    reserved "{"
    reserved "super"
    reserved "("
    ps2 <- many param
    reserved ")"
    reserved ";"
    as <- many assign
    reserved "}"
    return (Constructor n ps ps2 as)

method :: GenParser Char a Method
method =
 do
   r <- identifier
   n <- identifier
   reserved "("
   ps <- many param
   reserved ")"
   reserved "{"
   reserved "return"
   e <- expression
   reserved ";"
   reserved "}"
   return (Method r n ps e)

param :: GenParser Char a Param
param = do
  t <- identifier
  n <- identifier
  return (Param t n)

expression :: GenParser Char a Expression
expression =
  do
    a <- identifier
    return (Argument a)
  do
    n <- number
    return (Number n)
  <|>
  do
    reserved "this"
    reserved "."
    f <- identifier
    return (FieldCall f)
  <|>
  do
    reserved "this"
    reserved "."
    m <- identifier
    reserved "("
    ps <- many expression
    reserved ")"
    return (MethodCall f m ps)
  <|>
  do
    reserved "new"
    n <- identifier
    reserved "("
    ps <- many expression
    reserved ")"
    return (NewCall n ps)
    
lexer = P.makeTokenParser haskellDef

haskellDef = haskellStyle
            { P.reservedNames = [
              "class", "extends", "super", "this", "new",
              "=", ".", ",", ";", "(", ")", "{", "}"
              ], P.caseSensitive = True
            }

identifier :: GenParser Char a String
identifier = P.identifier lexer

reserved :: String -> GenParser Char a ()
reserved   = P.reserved lexer
