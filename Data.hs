module Data where

-- class with *ClassName* extends *ClassName*, has *[Field]*, one *Constructor*
-- and many *[Method]*
data Class = Class ClassName ClassName [Field] Constructor [Method] 
  deriving (Eq, Show)

-- field with *ClassName* has name *FieldName*
data Field = Field ClassName FieldName 
  deriving (Eq, Show)

-- constructor of *ClassName* with *[Param]* does super with *[Param]* a does
-- *[Assign]*
data Constructor = Constructor ClassName [Param] [Param] [Assign]
  deriving (Eq, Show)

-- method returns type *ClassName* has name *MethodName*, takes *[Param]* and
-- returns one *Expression*
data Method = Method ClassName MethodName [Param] Expression
  deriving (Eq, Show)

-- parameter is type of *ClassName* and its name is *FieldName*
data Param = Param ClassName FieldName
  deriving (Eq, Show)

-- Expression is either..
data Expression
  -- reference to method argument
  = Argument a
  -- plain number
  = Number number
  -- own field
  | FieldCall FieldName
  -- calling own method with params
  | MethodCall MethodName [Param]
  -- creating new object
  | NewCall ClassName [Param]
  deriving (Eq, Show)

type ClassName = String
type FieldName = String
type MethodName = String
